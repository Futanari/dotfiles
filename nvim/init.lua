lua = require('plugins')
local lsp = require "lspconfig"
local coq = require "coq"
require("nvim-autopairs").setup {}
require('lualine').setup()
require('onedark').load()

lsp.sumneko_lua.setup(coq.lsp_ensure_capabilities())
lsp.clangd.setup(coq.lsp_ensure_capabilities())

vim.cmd('COQnow -s')
vim.opt.number = true
vim.opt.cursorline = true
vim.opt.mouse = "a"



